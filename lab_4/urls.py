from django.urls import re_path

from .views import hello, home, message, saveproject, project_table, delete_table

app_name= 'lab_4'
urlpatterns = [
     re_path(r'^$', home, name='home'),
     re_path(r'^hello', hello, name='hello'),
     re_path(r'^message', message, name='message'),
     re_path(r'^saveproject', saveproject, name='saveproject'),
     re_path(r'^project_table', project_table, name='project_table'),
     re_path(r'^delete_table', delete_table, name='delete_table')
]
