from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Appointment
from .forms import Add_Project_Form

response = {}

def home(request):
    return render (request, 'index.html')

def hello(request):
    return render (request, 'sayhello.html')

def message(request):
    return render (request, 'register.html')

def saveproject(request):
    form = Add_Project_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        schedule = Appointment()
        schedule.title = form.cleaned_data['title']
        schedule.desc = form.cleaned_data['desc']
        schedule.created_date = form.cleaned_data['created_date']
        schedule.day = form.cleaned_data['day']
        schedule.place = form.cleaned_data['place']
        schedule.category = form.cleaned_data['category']
        schedule.save()
        html = 'formJadwal.html'
        return redirect('lab_4:project_table')

    else:
        form = Add_Project_Form()
        response = {'form' : form}
        return render(request, 'formJadwal.html', response)

def project_table(request):
    project = Appointment.objects.all()
    html = 'listJadwal.html'
    return render(request, html, {'project': project})

def delete_table(request):
    Appointment.objects.all().delete()
    project = Appointment.objects.all()
    html = 'listJadwal.html'
    return render(request, html, {'project': project})


# Create your views here.
