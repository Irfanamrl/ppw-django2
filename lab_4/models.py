from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.
class Appointment(models.Model):
    title = models.CharField(max_length=200, null=True)
    desc = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    day = models.CharField(max_length=30, null=True)
    place = models.CharField(max_length=200, null=True)
    category = models.CharField(max_length=30, null=True)
