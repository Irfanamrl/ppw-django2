from django import forms
from .models import Appointment

class Add_Project_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    date_attrs = {
        'type':'date',
        'class':'form-group col-xl-12',
        'placeholder':'date',
    }
    title_attrs = {
        'type': 'text',
        'class': 'form-group col-xl-12',
        'placeholder':'Title'
    }
    day_attrs = {
        'type': 'text',
        'class': 'form-group col-xl-12',
        'placeholder':'Day'
    }
    place_attrs = {
        'type': 'text',
        'class': 'form-group col-xl-12',
        'placeholder':'Place'
    }

    category_attrs = {
        'type': 'text',
        'class': 'form-group col-xl-12',
        'placeholder':'Category'
    }

    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'form-group col-xl-12',
        'placeholder':'Description'
    }

    title = forms.CharField(label='Title', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    created_date = forms.DateTimeField(label='Date', required=True, widget=forms.DateTimeInput(attrs=date_attrs))
    day = forms.CharField(label='Day', required=True, max_length=20, widget=forms.TextInput(attrs=day_attrs))
    place = forms.CharField(label='Place', required=True, max_length=200, widget=forms.TextInput(attrs=place_attrs))
    category = forms.CharField(label='Category', required=True, max_length=200, widget=forms.TextInput(attrs=category_attrs))
    desc = forms.CharField(label='Description', required=True, widget=forms.Textarea(attrs=description_attrs))
